import sqlite3
con = sqlite3.connect('sql1.db')
c = con.cursor()
c.execute(""" CREATE TABLE Departments(
deptname CHAR(60),
Deptid INTEGER UNIQUE)
""")
c.execute(""" CREATE TABLE EMPLOYEE(
Name CHAR(60),
Id INTEGER UNIQUE,
Salary INTEGER,
Deptid INTEGER)
""")
c.execute( "ALTER TABLE EMPLOYEE ADD COLUMN City CHAR(90)")

q1=" INSERT INTO EMPLOYEE ( Name,Id,Salary,Deptid,City ) VALUES (?,?,?,?,?)"
i=1
while(i<=5):
    print(" Employee number {}\t ".format(i))
    Name= input("Enter the Name of the employee : ")
    Id=input("Enter the Employee ID: ")
    Salary=input("Enter the Salary of the employee : ")
    Deptid=input("Enter the Department ID of the employee : ")
    City=input("Enter the City of the employee : ")
    c.execute(q1,( Name,Id,Salary,Deptid,City ))
    i=i+1
c.execute("SELECT Name ,Id ,Salary FROM EMPLOYEE")
print("Employee details: Name ,ID, Salary ",c.fetchall())
f=input("Enter the first letter of the name : ")
c.execute("SELECT * FROM EMPLOYEE WHERE Name LIKE'{}%'".format(f))
print("Employee full details who name start with {}".format(f),c.fetchall())
id= int(input("Enter the Employee Id :"))
c.execute("SELECT * FROM EMPLOYEE WHERE Id='{}'".format(id))
print("Employee full details who name start with {}".format(id),c.fetchall())
idu= int(input("Enter the Employee Id need to be updated :"))
name=input("Enter the Name need to be updated :")
c.execute("UPDATE EMPLOYEE SET Name='{}' WHERE Id='{}'".format(name,idu))
c.execute("SELECT * FROM EMPLOYEE WHERE Id='{}'".format(idu))
print("Updated name of the id {}".format(idu),c.fetchall())

q2=" INSERT INTO Departments ( deptname,Deptid ) VALUES (?,?)"
j=1
while(j<=5):
    print(" Department number {}\t ".format(j))
    deptname= input("Enter the Name of the department : ")
    Deptid=input("Enter the Department ID: ")
    c.execute(q2,( deptname,Deptid ))
    j=j+1
did = int(input("Enter the Department Id to be checked :"))
c.execute("SELECT Name,Id,Salary,City ,deptname FROM (EMPLOYEE E JOIN Departments D ON E.Deptid=D.Deptid) WHERE D.Deptid='{}' ".format(did))
print("Employee details of department id {} is ".format(did),c.fetchall())
con.commit()
con.close()
